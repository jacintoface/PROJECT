function animate(obj,json,duration,fn) {
    clearInterval(obj.timer);
    var initParams = getInit(obj,json,duration);
    var bTime = +new Date();
    obj.timer = setInterval(function () {
        var flag = true;
        for(var key in json){
            var nowTime = +new Date();
            var currentStyle = getStyle(obj,key)
            var disTime = nowTime - bTime;
            var dis = 0.5 * initParams.target[key] * disTime * disTime;
            if(disTime >= duration){
                obj.style[key] = json[key] +　getUnit(key);
            }else {
                obj.style[key] = initParams.init[key] + dis +　getUnit(key);
            }
            if(currentStyle !== json[key]){
                flag = false;
            }
        }
        if(flag){
            clearInterval(obj.timer)
            fn &&　fn();
        }
    },50)
}
function getInit(obj,json,duration) {
    var target = {},init = {};
    for(var key in json){
        var initStyle = getStyle(obj,key);
        var targetStyle = json[key];
        var speed = 2 *　(targetStyle - initStyle) / duration / duration;
        target[key] = speed;
        init[key] = initStyle;
    }
    return {target:target,init:init}
}

function getStyle(obj,attr) {
    if(obj.currentStyle){
        return parseFloat(obj.currentStyle[attr])
    }else {
        return parseFloat(window.getComputedStyle(obj,null)[attr])
    }
}
function getUnit(attr) {
    switch (attr) {
        case 'opacity':
            return '';
        default:
            return 'px';
    }
}