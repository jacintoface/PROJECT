(function (window,document,$) {
    /*将除第一张以外的图片放在最右边*/
    var box = document.querySelector('.y_slider_content_box')
    var slide = document.querySelectorAll('.y_inner_slider_main')
    var controller = document.querySelectorAll('.y_inner_slider_ctrl')
    var width = slide[0].children[0].clientWidth;
    for (var i = 0; i < slide.length; i++) {
        for(var k = 0;k < slide[i].children.length;k++) {
            slide[i].children[k].style.left = width + 'px';
        }
        slide[i].children[0].style.left = 0;
    }


    /*点击滑动图片*/
    var key = 0

    var letCtrl = document.querySelectorAll('.y_inner_slider_ctrl-left');
    var ritCtrl = document.querySelectorAll('.y_inner_slider_ctrl-right');
    for(var j = 0;j < letCtrl.length;j++) {

        letCtrl[j].onclick = (function (j) {
            return function () {
                leftslide(j);
            }
        })(j)

        ritCtrl[j].onclick = (function (j) {
            return function () {
                rightslide(j);
            }
        })(j)
        // ritCtrl[j].addEventListener('click',function () {
        //     console.log(j)
        //
        // })
    }

// box.addEventListener('click',function (e) {
//     var e = e||window.event;
//     var target = e.target||e.srcElement;
//     console.log(target)
//     if(target.className.indexOf("y_inner_slider_ctrl-left") > -1){
//         leftslide();
//     }else if(target.className.indexOf("y_inner_slider_ctrl-right") > -1){
//         rightslide();
//     }else {
//         console.log('我要跳转')
//     }
// })

//向右滑动函数
    function rightslide(j) {
        $(slide[j].children[key]).animate({'left': -width},400)
        //animate(slide.children[key],{'left': -width},500);
        ++key > slide[j].children.length -1 ? key = 0 : key;
        slide[j].children[key].style.left = width + 'px';
        $(slide[j].children[key]).animate({'left': 0},400)
        //animate(slide.children[key],{'left': 0},400);
    }

//向左滑动函数
    function leftslide(j) {
        $(slide[j].children[key]).animate({'left': width},400)
        //animate(slide.children[key],{'left': width},500);
        --key < 0 ? key = slide[j].children.length -1 : key;
        slide[j].children[key].style.left = -width + 'px';
        //animate(slide.children[key],{'left': 0},400);
        $(slide[j].children[key]).animate({'left': 0},400)
    }

//自动播放
    var timer = setInterval(auto,3000);
    function auto() {
        // rightslide();
    }

//控制定时器

    box.onmouseenter = function () {
        clearInterval(timer);
    }
    box.onmouseleave = function () {
        clearInterval(timer);
        timer = setInterval(auto,3000);
    }

})(window,document,$)