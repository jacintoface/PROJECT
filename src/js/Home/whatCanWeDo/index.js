(function (window, document, $) {
    $(function () {
        var ulBox = document.querySelector('.out-slide-box > ul')
        $('.iconlist_ul').children().each(function (item) {
            $('.iconlist_ul').children().eq(item).children('a').on('mouseenter', function () {
                if ($(window).width() > 1250) {
                    $(this).children('h3').css({color: '#fff'}).stop().animate({top: -50})
                    $(this).children('i').css({color: '#fff'})
                    $(this).children('p').css({color: 'rgba(255,255,255,1)'}).animate({opacity: 1}, 300)
                    $('.icon-mask').eq(item).stop().animate({height: 210}, 300)
                }
            })
        })
        $('.iconlist_ul').children().each(function (item) {
            $('.iconlist_ul').children().eq(item).children('a').on('mouseleave', function () {
                if ($(window).width() > 1250) {
                    $(this).children('p').css({color: 'rgba(255,255,255,0)'})
                    $(this).children('i').css({color: '#00dfb9'})
                    $(this).children('h3').css({color: '#333'}).stop().animate({top: 0})
                    $(this).prev().animate({height: 0}, 300, function () {
                    })
                }
            })
        })
        //var slideMask = document.querySelector('.slider-mask');
        //var sliderMaskBottom = document.querySelector('.slider-mask-bottom');
        /*单个li mouseover的一系列效果*/
        $('.y_slider_content_box').each(function () {
            $(this).find('.y_inner_slider_ctrl').css('opacity', 0)
            $(this).find('.slider-mask').animate({top: -275}, 100)
            $(this).find('.slider-mask-bottom').animate({top: 275}, 100)
            $(this).find('.in_slider_man_img').find('p').css({color: '#888'})
            $(this).find('.in_slider_man_img').find('h3').css({color: '#333'})
        })
        $('.y_slider_content_box').each(function (index) {
            $(this).hover(function (e) {
                $(this).find('.y_inner_slider_ctrl').css('opacity', 1)
                $(this).find('.slider-mask').animate({top: -87}, 100)
                $(this).find('.slider-mask-bottom').animate({top: 188}, 100)
                $(this).find('.in_slider_man_img').find('p').css({color: '#fff'})
                $(this).find('.in_slider_man_img').find('h3').css({color: '#fff'})
            }, function (e) {
                $(this).find('.y_inner_slider_ctrl').css('opacity', 0)
                $(this).find('.slider-mask').animate({top: -275}, 100)
                $(this).find('.slider-mask-bottom').animate({top: 275}, 100)
                $(this).find('.in_slider_man_img').find('p').css({color: '#888'})
                $(this).find('.in_slider_man_img').find('h3').css({color: '#333'})
            })
        })

        // ulBox.addEventListener('mouseenter',function (e) {
        //     var e = e || window.event;
        //     var target = e.target || e.srcElement;
        //     if($(target))
        // })
        // $('.y_slider_content_box').hover(function (e) {
        //     $('.y_inner_slider_ctrl').css("opacity",1)
        //     $('.slider-mask').animate({top:-87},100)
        //     $('.slider-mask-bottom').animate({top:188},100)
        //     $('.in_slider_man_img').find('p').css({color:'#fff'})
        //     $('.in_slider_man_img').find('h3').css({color:'#fff'})
        // },function () {
        //     $('.y_inner_slider_ctrl').css("opacity",0)
        //
        //     $('.slider-mask').animate({top:-275},100)
        //     $('.slider-mask-bottom').animate({top:275},100)
        //
        //     $('.in_slider_man_img').find('p').css({color:'#888'})
        //     $('.in_slider_man_img').find('h3').css({color:'#333'})
        // })

        // $('.y_inner_slider_ctrl').on('click',function () {
        //     window.location.href = 'http://localhost:3000/';
        // })
        //
        // $('.y_inner_slider_ctrl-left').on('click',function () {
        //     console.log(5555)
        // })

        // $('.y_inner_slider_ctrl').on('click',function (e) {
        //     e = e || window.event;
        //     var target = e.target || e.srcElement;
        //     console.log(target)
        //     if(target === this) {
        //         // window.location.href = 'http://www.baidu.com'
        //     }else if(target === $('.y_inner_slider_ctrl-left>i')[0]){
        //         console.log('left')
        //     }else if(target === $('.y_inner_slider_ctrl-right>i')[0]){
        //         console.log('right')
        //     }
        // })

        // $('.y_inner_slider_ctrl').on('mouseenter',function () {
        //     $('.y_inner_slider_ctrl').css({opacity:1})
        //     console.log('111111')
        // })
        /*out slider 部分*/
        var slideOut = document.querySelector('.out-slide-box > ul').children

        var outSlideBox = document.querySelector('.out-slide-box')

        var slideOutItemWidth = slideOut[0].clientWidth + 20

        var showImageTotal = Math.floor(outSlideBox.clientWidth / (slideOutItemWidth - 20))

        console.log(showImageTotal)

        // for(var l = 0;l < showImageTotal ;l++) {
        //     ulBox.appendChild(slideOut[l].cloneNode(true));
        // }
        //
        // for(var p = 0;p < showImageTotal ;p++) {
        //     ulBox.insertBefore(slideOut[slideOut.length - 1 - p].cloneNode(true),ulBox.children[0])
        // }

        if (showImageTotal === 3) {
            console.log(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[0])
        } else if (showImageTotal === 2) {
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[0])
            ulBox.removeChild(ulBox.children[0])
        } else if (showImageTotal === 1) {
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[ulBox.children.length - 1])
            ulBox.removeChild(ulBox.children[0])
            ulBox.removeChild(ulBox.children[0])
            ulBox.removeChild(ulBox.children[0])
        }

        ulBox.style.left = -showImageTotal * slideOutItemWidth + 'px'

        var outSlideKey = showImageTotal

        var outCtrl = document.querySelector('.slider_ctrl_out')
        outCtrl.onclick = function (e) {
            var e = e || window.event
            var target = e.target || e.srcElement
            if (target.className.indexOf('fa-angle-left') && target.className.indexOf('fa-angle-left') > -1) {

                leftslideOut()
            } else if (target.className.indexOf('fa-angle-right') && target.className.indexOf('fa-angle-right') > -1) {
                rightslideOut()
            }
        }

        function rightslideOut () {
            // if (showImageTotal === 2) {
            //     outSlideKey++
            //     console.log(outSlideKey, slideOut.length)
            //     if (outSlideKey >= 10) {
            //         $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400, function () {
            //             // ulBox.style.left = 0
            //             // outSlideKey = 0;
            //             // console.log('我执行了这列')
            //         })
            //     } else {
            //         $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400)
            //     }
            // } else {

            if (showImageTotal === 3) {
                outSlideKey++
                console.log(outSlideKey, slideOut.length)
                if (outSlideKey >= slideOut.length - showImageTotal) {
                    $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400, function () {
                        ulBox.style.left = -showImageTotal * slideOutItemWidth + 'px'
                        outSlideKey = showImageTotal
                        console.log('我执行了这列')
                    })
                } else {
                    $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400)
                }
            }else {
                outSlideKey++
                console.log(outSlideKey, slideOut.length)
                if (outSlideKey >= slideOut.length - showImageTotal) {
                    $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400, function () {
                        ulBox.style.left = -showImageTotal * slideOutItemWidth + 'px'
                        outSlideKey = showImageTotal
                        console.log('我执行了这列')
                    })
                } else {
                    $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400)
                }
            }

                // }
            // }



        }

        //向左滑动函数
        function leftslideOut () {
            outSlideKey--
            console.log(outSlideKey, slideOut.length)
            if (outSlideKey < 1) {
                $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400, function () {
                    console.log(slideOut.length - showImageTotal)
                    ulBox.style.left = -(slideOut.length - 2 * showImageTotal) * slideOutItemWidth + 'px'
                    // $(ulBox).css('left',0);
                    outSlideKey = slideOut.length - 2 * showImageTotal
                    console.log('我执行了这列2', outSlideKey)
                })
            } else {
                $(ulBox).stop().animate({'left': -outSlideKey * slideOutItemWidth}, 400)
            }
        }

        //自动播放
        // var timer = setInterval(auto,2000);
        // function auto() {
        //     rightslideOut();
        // }

//控制定时器

        // ulBox.onmouseenter = function () {
        //     clearInterval(timer);
        // }
        // ulBox.onmouseleave = function () {
        //     clearInterval(timer);
        //     timer = setInterval(auto,2000);
        // }
    })
})(window, document, $)