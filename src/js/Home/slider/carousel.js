var mySwiper = new Swiper('.section1', {
    pagination : '.swiper-pagination',
    paginationClickable :true,
    loop:true,
    nextButton:'.section1-next',
    prevButton:'.section1-prev',
    onInit: function(swiper){ //Swiper2.x的初始化是onFirstInit
        swiperAnimateCache(swiper); //隐藏动画元素
        swiperAnimate(swiper); //初始化完成开始动画
    },
    onSlideChangeEnd: function(swiper){
        swiperAnimate(swiper); //每个slide切换结束时也运行当前slide动画
    }
})
var swiper4 = new Swiper('.swiper4',{
    centeredSlides:true,
    grabCursor:true,
    loop:true,
    autoplay:3000,
    nextButton:'.section4-next',
    prevButton:'.section4-prev',
    paginationClickable :true,
})
var galleryTop = new Swiper('.gallery-top', {
    paginationClickable: true,
    spaceBetween: 30
})
var galleryThumbs = new Swiper('.gallery-thumbs',{
    slidesPerView: 'auto',
    centerdSliders:true,
    touchRatio:0.2,
    sliderToClickedSlide:true,
    spaceBetween: 30
})
galleryThumbs.params.control = galleryTop;
galleryTop.params.control = galleryThumbs;
var newSwiper = new Swiper('.news-text',{
    direction:'vertical',
    autoplay:3000,
    loop:true
})


