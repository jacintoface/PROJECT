/**
 * Created by luojian on 2017/10/15.
 */
var SummaySwiper = new Swiper('.content-summary',{
    pagination:'.summary-bullets',
    paginationClickable:true,
});
var AwardsSwiper = new Swiper('.content-awards',{
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    coverflow: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
    },
    initialSlide:2
});
var QualitySwiper = new Swiper('.content-quality',{
    pagination:'.quality-bullets',
    slidesPerView: 3,
    paginationClickable: true,
    spaceBetween: 88
});
var historySwiper = new Swiper('.history-content',{
    pagination: '.history-pagination',
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 50,
    centeredSlides:true,
    initialSlide:7,
    paginationType:'custom'
});